### The folder **qa_automation** contains scripts to build/run QA Automation Framework Scripts.

###### 1. Shell:

> docker_image_build.sh:
```bash
./docker_image_build.sh --subsystem auto_test --svcname auto_test --build-base-docker false --base-docker-image-name python3.8 --commit-id 83a0b0f82d9b69d258937628e834040b78128e1e --env '-' --region us-east-1
```

- **--build-base-docker** can accept one of following input values: **false | true**

> docker_image_run.sh:
```bash
./docker_image_run.sh --docker-image-tag '9fa8eed68416a9fd825021c725ca2e461bab8184' --automation-test-args '-s 1' --region us-east-1
```