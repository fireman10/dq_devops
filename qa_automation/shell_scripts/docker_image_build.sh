#!/bin/bash

#Input Parameters
declare SUBSYSTEM
declare SVC_NAME
declare COMMIT_ID
declare ENV
declare BUILD_BASE_DOCKER
declare BASE_DOCKER_IMAGE_NAME

declare REGION

#Custom Parameters
declare -r SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
declare DEPLOYMENT_FAILED="false"
declare -r BUILD_NO="dd_"`date +%Y%m%d%H%M%S`

declare SSM_DOCKER_UNAME_PATH
declare SSM_DOCKER_PWD_PATH

declare BASE_DOCKER_REPO
declare SUBSYSTEM_DOCKER_REPO
declare APP_BASE_DOCKER_IMAGE_NAME
declare APP_BASE_DOCKER_IMAGE_URI
declare APP_DOCKER_IMAGE_URI

#File Names/Paths
declare -r APP_BASE_DOCKER_FILE_PATH="$SCRIPTPATH/../docker_files"
declare APP_BASE_DOCKER_FILE_NAME
declare APP_DOCKER_FILE
declare SVC_FOLDER_PATH

#Funtion to Check if Previous Command Executed Successfully
function check_error() {
  if [ "${1}" -ne "0" ]; then
    DEPLOYMENT_FAILED="true"
    echo -e "ErrorCode#${1}; ${2} Exiting Script."
    exit ${1}
  else
    :
  fi
}

#Function to Validate Input Arguments passed and other "MAJOR" Logic/Validations
function validate_parameters() {
  echo "[[INFO]] Validating Input Parameters passed"
  while [[ "$#" > 0 ]]; do case $1 in
    --subsystem) SUBSYSTEM="$2";;
    --svcname) SVC_NAME="$2";;
    --build-base-docker) BUILD_BASE_DOCKER="$2";;
    --base-docker-image-name) BASE_DOCKER_IMAGE_NAME="$2";;
    --commit-id) COMMIT_ID="$2";;
    --env) ENV="$2";;
    --region) REGION="$2";;
    -h | --help) echo -e "\n[[USAGE]]\n$0 [--subsystem <auto_test>]* [--svcname <auto_test>]* [--build-base-docker <true/false>]* [--base-docker-image-name <python3.8>]* [--commit-id <9fa8eed68416a9fd825021c725ca2e461bab8184>]** [--env <devint/sqa>]* [--region <$REGION>]* || [-h | --help]"
    exit 1;;
    *) echo "[[ERROR]] Argument \"$1 $2\" was unexpected. Execute script with -h or --help to see usage."
      exit 1;;
  esac; shift; shift
  done

  if [[ -z $SUBSYSTEM ]]; then
    check_error 1 "[[ERROR]] --subsystem is a required parameter"
  fi
  if [[ -z $SVC_NAME ]]; then
    check_error 1 "[[ERROR]] --svcname is a required parameter"
  fi
  if [[ -z $BUILD_BASE_DOCKER ]]; then
    check_error 1 "[[ERROR]] --build-base-docker is a required parameter"
  fi
  if [[ -z $BASE_DOCKER_IMAGE_NAME ]]; then
    check_error 1 "[[ERROR]] --base-docker-image-name is a required parameter"
  fi
  if [[ -z $COMMIT_ID ]]; then
    echo "[[INFO]] Commit ID Value is NOT passed for $SVC_NAME Service, will get it run time later in \"initialize\" function."
  fi
  if [[ -z $ENV ]]; then
    check_error 1 "[[ERROR]] --env is a required parameter"
  fi
  if [[ -z $REGION ]]; then
    check_error 1 "[[ERROR]] --region is a required parameter"
  fi

  echo "[[INFO]] Validation Successful. Input Arguments passed: \"$SUBSYSTEM, $SVC_NAME, $BUILD_BASE_DOCKER, $BASE_DOCKER_IMAGE_NAME, $COMMIT_ID, $ENV, $REGION\""
}

#Function to Initialize Variables
function initialize() {
  echo "[[INFO]] Initializing Variables"

  SSM_DOCKER_UNAME_PATH="/dq/$REGION/config/global/dockerhub.username"
  SSM_DOCKER_PWD_PATH="/dq/$REGION/config/global/dockerhub.pwd"

  SVC_FOLDER_PATH="$SCRIPTPATH/../../../dq-$SUBSYSTEM-$SVC_NAME"

  APP_BASE_DOCKER_FILE_NAME="Dockerfile."$SUBSYSTEM"_basedocker"
  APP_DOCKER_FILE="$SVC_FOLDER_PATH/Dockerfile.$SVC_NAME"

  if [[ ! -f "$APP_BASE_DOCKER_FILE_PATH/$APP_BASE_DOCKER_FILE_NAME" ]]; then
    check_error 1 "[[ERROR]] Docker File DOESN'T exist. $APP_BASE_DOCKER_FILE_NAME ;"
  fi
  if [[ ! -s "$APP_BASE_DOCKER_FILE_PATH/$APP_BASE_DOCKER_FILE_NAME" ]]; then
    check_error 1 "[[ERROR]] Docker File exists but it's EMPTY. $APP_BASE_DOCKER_FILE_NAME ;"
  fi

  if [[ ! -f $APP_DOCKER_FILE ]]; then
    check_error 1 "[[ERROR]] Docker File DOESN'T exist. $APP_DOCKER_FILE ;"
  fi
  if [[ ! -s $APP_DOCKER_FILE ]]; then
    check_error 1 "[[ERROR]] Docker File exists but it's EMPTY. $APP_DOCKER_FILE ;"
  fi

  BASE_DOCKER_REPO="dataquser/base-docker-images"
  SUBSYSTEM_DOCKER_REPO="dataquser/$SUBSYSTEM"

  APP_BASE_DOCKER_IMAGE_NAME=$SUBSYSTEM"_"$BASE_DOCKER_IMAGE_NAME"_basedocker"

  APP_BASE_DOCKER_IMAGE_URI=$BASE_DOCKER_REPO:$APP_BASE_DOCKER_IMAGE_NAME

  if [[ -z $COMMIT_ID ]]; then
    echo "[[INFO]] Commit ID Value was NOT passed for $SVC_NAME Service as Input Argument. Getting Commit ID from git log in \"$SVC_FOLDER_PATH\" folder."
    COMMIT_ID=$(git --git-dir $SVC_FOLDER_PATH/.git log -1 | grep commit | head -1 | awk '{print $2}')
    if [[ -z $COMMIT_ID ]]; then
      check_error 1 "[[ERROR]] Failed while Getting Commit ID value from \"$SVC_FOLDER_PATH\" folder"
    fi
    echo "[[INFO]] Commit ID Value retrieved from git log is: $COMMIT_ID"
  fi

  APP_DOCKER_IMAGE_URI=$SUBSYSTEM_DOCKER_REPO:$COMMIT_ID

  echo "[[INFO]] Base Docker Repo: $BASE_DOCKER_REPO"
  echo "[[INFO]] Subsystem Docker Repo Name: $SUBSYSTEM_DOCKER_REPO"
  echo "[[INFO]] Application (Service) Base Docker Image URI: $APP_BASE_DOCKER_IMAGE_URI"
  echo "[[INFO]] Application (Service) Docker Image URI: $APP_DOCKER_IMAGE_URI"
}

#Function to login into Docker Hub
function docker_hub_login() {
  echo "[[INFO]] Getting Docker Login Details.."
  local DOCKER_UNAME="dataquser" #$(aws ssm get-parameters --names "$SSM_DOCKER_UNAME_PATH" --region $REGION | jq -r '.Parameters[].Value')
  local DOCKER_PWD="qasmoK-9tedcy-puzxus" #$(aws ssm get-parameters --names "$SSM_DOCKER_PWD_PATH" --with-decryption --region $REGION | jq -r '.Parameters[].Value')
  echo "[[INFO]] Logging in into Docker Hub"
  docker login --username $DOCKER_UNAME --password $DOCKER_PWD
  check_error $? "[[ERROR]] Failed while Logging in into Docker Hub."
  echo "[[INFO]] Login Successful"
  return
}

#Function to Push Docker Image
function push_docker_image(){
  echo "[[INFO]] Pushing Image: " $1
  docker push $1
  check_error $? "[[ERROR]] Failed while Pushing Docker Image: $1"
  echo "[[INFO]] Pushed Image Successfully, $1"
}

#Function to Build App/Subsystem Base Docker
function build_app_base_docker() {
  if [[ ! -f "$APP_BASE_DOCKER_FILE_PATH/$APP_BASE_DOCKER_FILE_NAME" ]]; then
    check_error 1 "[[ERROR]] Docker File DOESN'T exist. $APP_BASE_DOCKER_FILE_NAME ;"
  fi
  if [[ ! -s "$APP_BASE_DOCKER_FILE_PATH/$APP_BASE_DOCKER_FILE_NAME" ]]; then
    check_error 1 "[[ERROR]] Docker File exists but it's EMPTY. $APP_BASE_DOCKER_FILE_NAME ;"
  fi

  echo "[[INFO]] DELETING existing Application_BaseDocker Image: $APP_BASE_DOCKER_IMAGE_URI"
  docker rmi -f $APP_BASE_DOCKER_IMAGE_URI
  echo "[[INFO]] $APP_BASE_DOCKER_IMAGE_URI Image Deleted"

  echo "[[INFO]] Copying $APP_BASE_DOCKER_FILE_PATH/$APP_BASE_DOCKER_FILE_NAME Dockerfile to $SVC_FOLDER_PATH"
  cp -r $APP_BASE_DOCKER_FILE_PATH/$APP_BASE_DOCKER_FILE_NAME $SVC_FOLDER_PATH

  echo "[[INFO]] Building Application BaseDocker Image again as selected: $APP_BASE_DOCKER_IMAGE_URI"
  docker build -t $APP_BASE_DOCKER_IMAGE_URI -f=$SVC_FOLDER_PATH/$APP_BASE_DOCKER_FILE_NAME .
  check_error $? "[[ERROR]] Failed while Building Docker Image: $APP_BASE_DOCKER_IMAGE_URI"

  push_docker_image $APP_BASE_DOCKER_IMAGE_URI
  rm -rf $SVC_FOLDER_PATH/$APP_BASE_DOCKER_FILE_NAME
}

#Function to Build App/Service Docker Image
function build_app_docker_image() {
  echo "[[INFO]] Building Application/Service Docker Image: $APP_DOCKER_IMAGE_URI from Docker File: $APP_DOCKER_FILE"
  docker build -t $APP_DOCKER_IMAGE_URI -f=$APP_DOCKER_FILE .
  check_error $? "[[ERROR]] Failed while Building Docker Image: $APP_DOCKER_IMAGE_URI"
  push_docker_image $APP_DOCKER_IMAGE_URI
}

# Main Function
function main() {
  validate_parameters "$@"

  initialize

  docker_hub_login

  cd $SVC_FOLDER_PATH

  if [[ $BUILD_BASE_DOCKER == "true" || $BUILD_BASE_DOCKER == "TRUE" ]]; then
    build_app_base_docker
  fi

  build_app_docker_image

  if [[ $DEPLOYMENT_FAILED == "false" ]]; then
    echo -e "\n==> $0 SCRIPT EXECUTION IS \"SUCCESSFUL\"."
    return
  fi
}

main "$@"
