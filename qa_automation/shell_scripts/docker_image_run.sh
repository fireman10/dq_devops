#!/bin/bash

#Input Parameters
declare DOCKER_IMAGE_TAG
declare AUTOMATION_TEST_ARGS

declare REGION

#Custom Parameters
declare -r SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
declare DEPLOYMENT_FAILED="false"
declare -r BUILD_DATE="dd_"`date +%Y%m%d%H%M%S`
declare -r SUBSYSTEM_DOCKER_REPO="dataquser/auto_test"
declare -r AUTOMATION_SCRIPT_FILE="python dataq.py"
declare AUTOMATION_RUN_CMD
declare SSM_DOCKER_UNAME_PATH
declare SSM_DOCKER_PWD_PATH

#Funtion to Check if Previous Command Executed Successfully
function check_error() {
  if [ "${1}" -ne "0" ]; then
    DEPLOYMENT_FAILED="true"
    echo -e "ErrorCode#${1}; ${2} Exiting Script."
    exit ${1}
  else
    :
  fi
}

#Function to Validate Input Arguments passed and other "MAJOR" Logic/Validations
function validate_parameters() {
  echo "[[INFO]] Validating Input Parameters passed"
  while [[ "$#" > 0 ]]; do case $1 in
    --docker-image-tag) DOCKER_IMAGE_TAG="$2";;
    --automation-test-args) AUTOMATION_TEST_ARGS="$2";;
    --region) REGION="$2";;
    -h | --help) echo -e "\n[[USAGE]]\n$0 [--docker-image-tag <9fa8eed68416a9fd825021c725ca2e461bab8184>]* [--automation-test-args <-s 1/ -s 1,2>]* [--region <us-east-1>]* || [-h | --help]" && \
    echo -e "[[USAGE-INFO]] Options listed with \"* are MANDATORY\" and Options listed with \"** are OPTIONAL\".\n"
    exit 1;;
    *) echo "[[ERROR]] Argument \"$1 $2\" was unexpected. Execute script with -h or --help to see usage."
      exit 1;;
  esac; shift; shift
  done

  if [[ -z $DOCKER_IMAGE_TAG ]]; then
    check_error 1 "[[ERROR]] --docker-image-tag is a required parameter"
  fi
  if [[ -z $AUTOMATION_TEST_ARGS ]]; then
    check_error 1 "[[ERROR]] --automation-test-args is a required parameter"
  fi
  if [[ -z $REGION ]]; then
    check_error 1 "[[ERROR]] --region is a required parameter"
  fi

  echo "[[INFO]] Validation Successful. Input Arguments passed: \"$DOCKER_IMAGE_TAG, $AUTOMATION_TEST_ARGS, $REGION\""
}

#Function to Initialize Variables
function initialize() {
  echo "[[INFO]] Initializing Variables"

  SSM_DOCKER_UNAME_PATH="/dq/$REGION/config/global/dockerhub.username"
  SSM_DOCKER_PWD_PATH="/dq/$REGION/config/global/dockerhub.pwd"

  AUTOMATION_RUN_CMD="$AUTOMATION_SCRIPT_FILE $AUTOMATION_TEST_ARGS"
}

#Function to login into Docker Hub
function docker_hub_login() {
  echo "[[INFO]] Getting Docker Login Details."
  local DOCKER_UNAME="dataquser" #$(aws ssm get-parameters --names "$SSM_DOCKER_UNAME_PATH" --region $REGION | jq -r '.Parameters[].Value')
  local DOCKER_PWD="qasmoK-9tedcy-puzxus" #$(aws ssm get-parameters --names "$SSM_DOCKER_PWD_PATH" --with-decryption --region $REGION | jq -r '.Parameters[].Value')
  echo "[[INFO]] Logging in into Docker Hub"
  docker login --username $DOCKER_UNAME --password $DOCKER_PWD
  check_error $? "[[ERROR]] Failed while Logging in into Docker Hub."
  echo "[[INFO]] Login Successful"
  return
}

#Function to Pull Docker Image
function pull_docker_image(){
  echo "[[INFO]] Pulling Image: " $1
  docker pull $1
  check_error $? "[[ERROR]] Failed while Pulling Docker Image: $1"
  echo "[[INFO]] Pulled Image Successfully, $1"
}

#Function to Run Docker Image
function docker_run(){
  echo "[[INFO]] Running Docker Image: \"$1\" with CMD/ARGS \"$2\""
  echo -e "\n************************************************************************"
  echo -e "[[INFO]] Test Cases Output will be printed below:\n"
  # docker run -d $1 $2
  docker run $1 $2
  check_error $? "[[ERROR]] Failed while Running Docker Image: $1"
  echo -e "\n************************************************************************\n"
  echo "[[INFO]] Docker Image Run was Successful."
}

#Function to Cleanup/Remove ALL Stopped Docker Containers/Images
function docker_cleanup(){
  echo -e "\n[[INFO]] Removing ALL Stopped Docker Containers"
  docker rm $(docker ps -a -q) || true
  echo "[[INFO]] Pruning ALL Dangling Docker Images"
  docker image prune -f || true
  echo -e "\n**[[NOTE]] Docker Containers and Images Cleanup/Pruning is NOT always guaranteed. Please verify manually as well."
}

# Main Function
function main() {
  validate_parameters "$@"

  initialize

  docker_hub_login

  pull_docker_image "$SUBSYSTEM_DOCKER_REPO:$DOCKER_IMAGE_TAG"

  docker_run "$SUBSYSTEM_DOCKER_REPO:$DOCKER_IMAGE_TAG" "$AUTOMATION_RUN_CMD"

  docker_cleanup

  if [[ $DEPLOYMENT_FAILED == "false" ]]; then
    echo -e "\n==> $0 SCRIPT EXECUTION IS \"SUCCESSFUL\"."
    return
  fi
}

main "$@"
