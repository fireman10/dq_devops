#!/bin/bash

#Input Parameters
declare APP_VERSION
declare UI_VERSION
declare TARGET_EC2_INSTANCE_IDS
declare SSM_OUTPUT_CW_LOG_GROUP
declare ENV

#Custom Parameters
declare -r SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
declare DEPLOYMENT_FAILED="false"

declare -r REGION="us-east-1"

declare -r COMMAND_COMMENT="Updating either or both of APP and UI Docker Image Versions"
declare -r EXECUTION_TIMEOUT="5400"
declare -r COMMAND_TIMEOUT_SECONDS="600"
declare -r SLEEP_TIME_SECONDS="30"

#File Names/Folder Paths
declare -r SSM_COMMAND_WORKING_DIRECTORY="/home/ubuntu/testing_whiz_pro"

#Commands
declare UPDATE_ENV_APP_VERSION_CMD
declare UPDATE_ENV_UI_VERSION_CMD
declare UPDATE_ENV_CMD

declare DOCKER_STOP_CMD
declare DOCKER_START_CMD
declare DOCKER_PRUNE_CMD

#Funtion to Check if Previous Command Executed Successfully
function check_error() {
  if [ "${1}" -ne "0" ]; then
    DEPLOYMENT_FAILED="true"
    echo -e "ErrorCode#${1}; ${2} Exiting Script."
    exit ${1}
  else
    :
  fi
}

#Function to Validate Input Arguments passed and other "MAJOR" Logic/Validationsfunction
function validate_parameters() {
    echo "[[INFO]] Validating Input Parameters passed"
    while [[ "$#" > 0 ]]; do case $1 in
        --app-version) APP_VERSION="$2";;
        --ui-version) UI_VERSION="$2";;
        --target_ec2_instance_ids) TARGET_EC2_INSTANCE_IDS="$2";;
        --ssm-output-cw-log-group) SSM_OUTPUT_CW_LOG_GROUP="$2";;
        --env) ENV="$2";;
        -h | --help) echo -e "\n[[USAGE]]\n$0 [--app-version <54-73f3d0a444>]* [--ui-version <27-73f3d0a444>]* [--target_ec2_instance_ids <i-0c4c4bff829edbf61>]* [--ssm-output-cw-log-group <dq-ssm-logs-dev>]* [--env <dev/sqa/prd>]* || [-h | --help]"
        exit 1;;
        *) echo "[[ERROR]] Argument \"$1 $2\" was unexpected. Execute script with -h or --help to see usage."
          exit 1;;
    esac; shift; shift
    done

    if [[ -z $APP_VERSION ]]; then
      echo -e "[[WARNING]] --app-version parameter value is NOT passed."
    fi
    if [[ -z $UI_VERSION ]]; then
      echo -e "[[WARNING]] --ui-version parameter value is NOT passed."
    fi
    if [[ -z $TARGET_EC2_INSTANCE_IDS ]]; then
      check_error 1 "[[ERROR]] --target_ec2_instance_ids is a required parameter."
    fi
    if [[ -z $SSM_OUTPUT_CW_LOG_GROUP ]]; then
      check_error 1 "[[ERROR]] --ssm-output-cw-log-group is a required parameter."
    fi
    if [[ -z $ENV ]]; then
      echo -e "[[WARNING]] --env parameter value is NOT passed."
    fi

    echo "[[INFO]] Validation Successful. Input Arguments passed: \"$APP_VERSION, $UI_VERSION, $TARGET_EC2_INSTANCE_IDS, $SSM_OUTPUT_CW_LOG_GROUP, $ENV\""
}

#Function to Initialize Variables
function initialize(){
  if [[ -z $APP_VERSION && -z $UI_VERSION ]] || [[ $APP_VERSION == "-" && $UI_VERSION == "-" ]]; then
    check_error 1 "[[ERROR]] Both APP_VERSION and UI_VERSION parameter values are NOT passed. Nothing to do."
  fi

  UPDATE_ENV_APP_VERSION_CMD="sed -i "s/^APP_VERSION.*$/APP_VERSION=$APP_VERSION/" .env"
  UPDATE_ENV_UI_VERSION_CMD="sed -i "s/^UI_VERSION.*$/UI_VERSION=$UI_VERSION/" .env"
  
  if [[ ! -z $APP_VERSION && $APP_VERSION != "-" ]] && [[ -z $UI_VERSION || $UI_VERSION == "-" ]]; then
    UPDATE_ENV_CMD="$UPDATE_ENV_APP_VERSION_CMD"
  elif [[ -z $APP_VERSION || $APP_VERSION == "-" ]] && [[ ! -z $UI_VERSION && $UI_VERSION != "-" ]]; then
    UPDATE_ENV_CMD="$UPDATE_ENV_UI_VERSION_CMD"
  elif [[ ! -z $APP_VERSION && ! -z $UI_VERSION ]] && [[ $APP_VERSION != "-" && $UI_VERSION != "-" ]]; then
    UPDATE_ENV_CMD="$UPDATE_ENV_APP_VERSION_CMD && $UPDATE_ENV_UI_VERSION_CMD"
  else
    check_error 1 "[[ERROR]] Something went wrong while setting \"UPDATE_ENV_CMD\" variable value in \"initialize\" function."
  fi

  if [[ -z $UPDATE_ENV_CMD ]]; then
    check_error 1 "[[ERROR]] \"UPDATE_ENV_CMD\" variable value is null/empty. This is NOT expected. Please verify."
  fi

  DOCKER_STOP_CMD="sudo docker-compose -f twp.docker-compose.yml down"
  DOCKER_START_CMD="sudo docker-compose -f twp.docker-compose.yml up -d"
  # DOCKER_PRUNE_CMD="sudo docker image prune -a -f"
  DOCKER_PRUNE_CMD="sudo docker image prune -f"
}

#Function to Execute SSM "send-command" CLI Command
function ssm_send_command() {
  echo "[[INFO]] Executing \"ssm send-command\""

  aws ssm send-command --region $REGION --document-name "AWS-RunShellScript" --document-version "1" \
  --targets "Key=InstanceIds,Values=$TARGET_EC2_INSTANCE_IDS" \
  --parameters workingDirectory=$SSM_COMMAND_WORKING_DIRECTORY,executionTimeout=$EXECUTION_TIMEOUT,commands=["echo ==========VERSIONS BEFORE UPDATE=========; head -3 .env; $UPDATE_ENV_CMD && echo ==========VERSIONS AFTER UPDATE=========; head -3 .env; echo ==========STOPPING DOCKER CONTAINERS=========; $DOCKER_STOP_CMD && echo ==========WAITING $SLEEP_TIME_SECONDS SECONDS TO START DOCKER CONTAINERS=========; sleep $SLEEP_TIME_SECONDS; echo ==========STARTING DOCKER CONTAINERS=========; $DOCKER_START_CMD && echo ==========PRUNING DOCKER IMAGES=========; $DOCKER_PRUNE_CMD"] \
  --comment "$COMMAND_COMMENT" --timeout-seconds $COMMAND_TIMEOUT_SECONDS \
  --max-concurrency 1 --max-errors 1 \
  --cloud-watch-output-config CloudWatchOutputEnabled=true,CloudWatchLogGroupName="$SSM_OUTPUT_CW_LOG_GROUP"
  # --service-role-arn "arn:aws:iam::<>"

  check_error $? "[[ERROR]] Error while executing \"ssm_send_command\" function."
  return
}

function main() {
  validate_parameters "$@"

  initialize

  ssm_send_command

  if [[ $DEPLOYMENT_FAILED == "false" ]]; then
    echo -e "\n==> $0 SCRIPT EXECUTION IS \"SUCCESSFUL\". Please verify."
    return
  fi
}

main "$@"
