### The folder **shellscripts** contains scripts to do various operations. See below for more details.

##### 1. Docker Image Deploy/Update Script (ssm_sendcommand.sh):

##### a. Shell:

```bash
./ssm_sendcommand.sh --app-version 56-6ddb38e4e5 --ui-version 27-73f3d0a444 --target_ec2_instance_ids i-0c4c4bff829edbf61 --ssm-output-cw-log-group dq-ssm-logs-dev --env dev
```

- If there are NO Updates to either --app-version or --ui-version, pass the corresponding argument value as '-' (or ignore completely).

##### b. Pre-req for script to work:
- SSM Agent must be installed on the EC2 host
    - https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-install-managed-linux.html
      - Using Snap packages
  - IAM Role Used: **docker-dev-ec2-role**
  - SSM Permissions Policies attached:
    - GOV_ssm_basic (Managed policy)
    - ec2-ssm-policy (Managed policy)

- AWS Services Used in this solution:
    - AWS CLI
    - AWS Systems Manager
    - CloudWatch LogGroup (**dq-ssm-logs-dev**)


##### 2. EC2 Start/Stop/Reboot Script (ec2_start_stop.sh):

##### a. Shell:

```bash
./ec2_start_stop.sh --ec2-action stop --ec2-instance-ids i-09e585c461ffe49e4 --ec2-instance-name Dataq-docker-Prod --env dev
```

- Valid values for --ec2-action are start/stop/reboot.
- One of the following is a required argument: **--ec2-instance-ids** (preferred) || **--ec2-instance-name**
- If both are passed, **--ec2-instance-ids** takes precedence and will be used.
- If there are any new Instance Names/Instance Ids, add them to the ./ec2_list.txt file
