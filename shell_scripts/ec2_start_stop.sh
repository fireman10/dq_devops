#!/bin/bash

#Input Parameters
declare EC2_ACTION
declare EC2_INSTANCE_IDS
declare EC2_INSTANCE_NAME
declare ENV

#Custom Parameters
declare -r SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
declare DEPLOYMENT_FAILED="false"

declare -r REGION="us-east-1"

#File Names/Folder Paths
declare EC2_LIST_FILE="$SCRIPTPATH/ec2_list.txt"

#Commands
declare EC2_CMD

#Funtion to Check if Previous Command Executed Successfully
function check_error() {
  if [ "${1}" -ne "0" ]; then
    DEPLOYMENT_FAILED="true"
    echo -e "ErrorCode#${1}; ${2} Exiting Script."
    exit ${1}
  else
    :
  fi
}

#Function to Validate Input Arguments passed and other "MAJOR" Logic/Validationsfunction
function validate_parameters() {
    echo "[[INFO]] Validating Input Parameters passed"
    while [[ "$#" > 0 ]]; do case $1 in
        --ec2-action) EC2_ACTION="$2";;
        --ec2-instance-ids) EC2_INSTANCE_IDS="$2";;
        --ec2-instance-name) EC2_INSTANCE_NAME="$2";;
        --env) ENV="$2";;
        -h | --help) echo -e "\n[[USAGE]]\n$0 [--ec2-action <start/stop/reboot>]* [--ec2-instance-ids <i-09e585c461ffe49e4/i-0c4c4bff829edbf61 i-0890c10c47a2a9b80>]** [--ec2-instance-name <Dataq-docker-Prod/docker-dev>]** [--env <dev/sqa/prd>]* || [-h | --help]"
        exit 1;;
        *) echo "[[ERROR]] Argument \"$1 $2\" was unexpected. Execute script with -h or --help to see usage."
          exit 1;;
    esac; shift; shift
    done

    if [[ -z $EC2_ACTION ]]; then
      check_error 1 "[[ERROR]] --ec2-action is a required parameter."
    fi
    if [[ -z $EC2_INSTANCE_IDS ]]; then
      echo -e "[[WARNING]] --ec2-instance-ids parameter value is NOT passed. Checking if \"--ec2-instance-name\" parameter is passed."
      if [[ -z $EC2_INSTANCE_NAME ]]; then
        check_error 1 "[[ERROR]] \"--ec2-instance-name\" parameter is NOT passed as well. One of the following parameters is required, \"--ec2-instance-ids\" || \"--ec2-instance-name\" ;"
      fi
    fi
    if [[ -z $ENV ]]; then
      echo -e "[[WARNING]] --env parameter value is NOT passed."
    fi

    echo "[[INFO]] Validation Successful. Input Arguments passed: \"$EC2_ACTION, $EC2_INSTANCE_IDS, $EC2_INSTANCE_NAME,  $ENV\""
}

#Function to get EC2 Instance ID for given EC2 Instance Name from $EC2_LIST_FILE
function get_instance_id() {
  if grep -Fq "$1" "$2"
  then
    echo $(awk /"^$1="/{print} "$2" | cut -d '=' -f2)
  else
    echo ""
  fi
}

#Function to Initialize Variables
function initialize(){
  EC2_ACTION=$(echo "$EC2_ACTION" | tr '[:upper:]' '[:lower:]')

  if [[ ! -z $EC2_INSTANCE_IDS && ! -z $EC2_INSTANCE_NAME ]]; then
    echo -e "[[INFO]] Both EC2 Instance IDs and Instance Name Parameters are passed. To \"$EC2_ACTION\" the instance(s), EC2 Instance IDs take precedence and ONLY this will be used: $EC2_INSTANCE_IDS (Instance Names will be ignored)."
  elif [[ -z $EC2_INSTANCE_IDS && ! -z $EC2_INSTANCE_NAME ]]; then
    if [[ ! -f $EC2_LIST_FILE ]]; then
      check_error 1 "**[[ERROR]] EC2_LIST_FILE does NOT exist at path: $EC2_LIST_FILE"
    elif [[ ! -s $EC2_LIST_FILE ]]; then
      check_error 1 "**[[ERROR]] EC2_LIST_FILE exists but has NO Contents/Data at path: $EC2_LIST_FILE"
    elif [[ -s $EC2_LIST_FILE ]]; then
      echo "[[INFO]] Getting Instance ID for given Instance Name: \"$EC2_INSTANCE_NAME\" from the file: $EC2_LIST_FILE"
      EC2_INSTANCE_IDS=$(get_instance_id $EC2_INSTANCE_NAME $EC2_LIST_FILE)
    fi
  fi

  if [[ -z $EC2_INSTANCE_IDS ]]; then
    check_error 1 "**[[ERROR]] EC2 Instance Id NOT found for given name, \"$EC2_INSTANCE_NAME\" at path: $EC2_LIST_FILE ; This is NOT expected. Please verify and try again."
  else
    echo -e "[[INFO]] EC2_INSTANCE_IDS value found: $EC2_INSTANCE_IDS"
  fi
}

#Function to Execute EC2 CLI Command
function run_ec2_command() {
  echo "[[INFO]] Performing \"$EC2_ACTION\" action on Instance Ids: \"$EC2_INSTANCE_IDS\""

  aws ec2 $EC2_ACTION-instances --region $REGION --instance-ids $EC2_INSTANCE_IDS

  check_error $? "[[ERROR]] Error while executing \"$EC2_ACTION\" on Instance Ids: \"$EC2_INSTANCE_IDS\""
  return
}

function main() {
  validate_parameters "$@"

  initialize

  run_ec2_command

  if [[ $DEPLOYMENT_FAILED == "false" ]]; then
    echo -e "\n==> $0 SCRIPT EXECUTION IS \"SUCCESSFUL\". Please verify."
    return
  fi
}

main "$@"
