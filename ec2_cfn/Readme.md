##### 1. Script Pre-Execution Requirements:
- Follow the placeholders and Update the CFN Parameters File in ./cfn/cfn_templates_parameters/infrastructure/env.account.region.ec2-infra.cfn.parameters.json. Below five properties are mandatory. 
  * VPCId
  * PrivateSubnets
  * PublicSubnets
  * KeyPairName
  * ALBSSLPolicy
  * ACMCertificateArn

- Update the CFN Stack Tags File as needed in ./cfn/cfn_templates_parameters/common/env.account.region.cfn-stack.tags.json
- Update both the Parameter and Stack Tags files name ( env.account.region.emr-infra.cfn.parameters.json and env.account.region.cfn-stack.tags.json ) with corresponding env/account/region values you need
- Update **sed** command under **replace_template_parameters()** function in the script(ec2_cfn/shell_scripts/ec2_infra_deploy.sh) as below,
  - If running the scripts in Mac, UPDATE all 'sed' commands in below Format:
    > sed -i '' "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE
  - If running the scripts in LINUX, UPDATE all 'sed' commands in below Format:
    > sed -i "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE

##### 2. Software Requirements:
- Pleaes make sure **jq** and **aws cli** are installed in the machine where the scripts are being exeucted from.

##### 3. Execute Infrastructure Shell Script:
> Below example will create infra for **dev** environment and account **dataq** with **internet-facing (public)** Application Load Balancer and **http** protocol

```bash
$ ./ec2_infra_deploy.sh --stack-tag dataq --subsystem app --env dev --account dataq --region us-east-1 --ec2-ami-id 'ami-0947d2ba12ee1ff75' --alb-scheme 'internet-facing' --alb-protocol 'http' --create-r53 'false' --r53-hosted-zone-id '' --r53-record-set-name ''
```
> NOTE:
- --alb-scheme value must be only one of the following: **internal (or) internet-facing**
- --alb-protocol value must be only one of the following: **http (or) https**
- --create-r53 value must be only one of the following: **true (or) false**
- *--r53-hosted-zone-id* and *--r53-record-set-name* are **optional arguments** which are **required only** when *--create-r53 value is true*
- Recommended to use Latest Amazon Linux 2 EC2 AMI, when you can.
