#!/bin/bash

declare STACK_TAG
declare SUBSYSTEM
declare ENV
declare EC2_AMI_ID
declare ALB_SCHEME
declare ALB_PROTOCOL
declare CREATE_R53
declare R53_HOSTED_ZONE_ID
declare R53_RECORD_SET_NAME

declare ACCOUNT
declare REGION

declare -r SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
declare DEPLOYMENT_FAILED="false"
declare CFN_STACK_NAME
declare CFN_STACK_STATUS

declare -r INFRA_CFN_TEMPLATE_FILE="$SCRIPTPATH/../cfn/cfn_templates/ec2_infra_cfn.yml"
declare -r CFN_VALIDATE_LOG=success.log
declare PARAMETER_FILE
declare INFRA_PARAMETER_FILE
declare CFN_TAGS_FILE

function check_error() {
  if [ "${1}" -ne "0" ]; then
    DEPLOYMENT_FAILED="true"
    echo -e "ErrorCode#${1}; ${2} Exiting Script."
    exit ${1}
  else
    return
  fi
}

function validate_parameters() {
    while [[ "$#" > 0 ]]; do case $1 in
        --stack-tag) STACK_TAG="$2";;
        --subsystem) SUBSYSTEM="$2";;
        --env) ENV="$2";;
        --ec2-ami-id) EC2_AMI_ID="$2";;
        --alb-scheme) ALB_SCHEME="$2";;
        --alb-protocol) ALB_PROTOCOL="$2";;
        --create-r53) CREATE_R53="$2";;
        --r53-hosted-zone-id) R53_HOSTED_ZONE_ID="$2";;
        --r53-record-set-name) R53_RECORD_SET_NAME="$2";;
        --account) ACCOUNT="$2";;
        --region) REGION="$2";;
        -h | --help) echo -e "\n[[USAGE]]\n$0 [--stack-tag <dataq>]* [--subsystem <app>]* [--env <dev/sqa/prd>]* [--ec2-ami-id <ami-0947d2ba12ee1ff75>]* [--alb-scheme <internal/internet-facing>]* [--alb-protocol <http/https]* [--create-r53 <true/false>]* [--r53-hosted-zone-id <Z35SXDOTRQ7X7K>]** [--r53-record-set-name <dataqapp.dev.foo.com>]** [--account <dataq>]* [--region <us-east-1>]* || [-h | --help]"
        echo -e "\n[[INFO]]\n\"*\" means Required Parameter\n\"**\" means Required parameter ONLY for certain Environments/Cases."
          exit 1;;
        *) echo "[[ERROR]] Argument \"$1 $2\" was unexpected. Execute script with -h or --help to see usage."
          exit 1;;
    esac; shift; shift
    done

    if [[ -z $STACK_TAG ]]; then
      check_error 1 "[[ERROR]] --stack-tag is a required parameter"
    fi
    if [[ -z $SUBSYSTEM ]]; then
      check_error 1 "[[ERROR]] --subsystem is a required parameter"
    fi
    if [[ -z $ENV ]]; then
      check_error 1 "[[ERROR]] --env is a required parameter"
    fi
    if [[ -z $EC2_AMI_ID ]]; then
      check_error 1 "[[ERROR]] --ec2-ami-id is a required parameter for SVC_LAUNCH_TYPE: $SVC_LAUNCH_TYPE"
    fi
    if [[ -z $ALB_SCHEME ]]; then
      check_error 1 "[[ERROR]] --alb-scheme is a required parameter"
    elif [[ $ALB_SCHEME != "internal" && $ALB_SCHEME != "internet-facing" ]]; then
      check_error 1 "[[ERROR]] --alb-scheme value must be only one of the following: \"internal\" or \"internet-facing\";"
    fi
    if [[ -z $ALB_PROTOCOL ]]; then
      check_error 1 "[[ERROR]] --alb-protocol is a required parameter"
    elif [[ $ALB_PROTOCOL != "http" &&  $ALB_PROTOCOL != "https" ]]; then
      check_error 1 "[[ERROR]] --alb-protocol value must be only one of the following: \"http\" or \"https\";"
    fi
    if [[ -z $CREATE_R53 ]]; then
      check_error 1 "[[ERROR]] --create-r53 is a required parameter"
    elif [[ $CREATE_R53 != "true" &&  $CREATE_R53 != "false" ]]; then
      check_error 1 "[[ERROR]] --create-r53 value must be only one of the following: \"true\" or \"false\";"
    fi
    if [[ $CREATE_R53 == "true" && -z $R53_HOSTED_ZONE_ID ]]; then
      check_error 1 "[[ERROR]] --r53-hosted-zone-id is a required parameter when --create-r53 value is: \"$CREATE_R53\""
    fi
    if [[ $CREATE_R53 == "true" && -z $R53_RECORD_SET_NAME ]]; then
      check_error 1 "[[ERROR]] --r53-record-set-name is a required parameter when --create-r53 value is: \"$CREATE_R53\""
    fi
    if [[ -z $ACCOUNT ]]; then
      check_error 1 "[[ERROR]] --account is a required parameter"
    fi
    if [[ -z $REGION ]]; then
      check_error 1 "[[ERROR]] --region is a required parameter"
    fi
}

function convert_to_lower() {
  SUBSYSTEM=$(echo "$SUBSYSTEM" | tr '[:upper:]' '[:lower:]')
  ACCOUNT=$(echo "$ACCOUNT" | tr '[:upper:]' '[:lower:]')
  REGION=$(echo "$REGION" | tr '[:upper:]' '[:lower:]')
}

function initialize(){

  INFRA_PARAMETER_FILE="$SCRIPTPATH/../cfn/cfn_templates_parameters/infrastructure/$ENV.$ACCOUNT.$REGION.ec2-infra.cfn.parameters.json"
  CFN_TAGS_FILE="$SCRIPTPATH/../cfn/cfn_templates_parameters/common/$ENV.$ACCOUNT.$REGION.cfn-stack.tags.json"
}

function replace_template_parameters () {
  PARAMETER_FILE=$1

  ## For Mac UPDATE all 'sed' commands in below Format:
  ## sed -i '' "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE
  ## For Linux UPDATE all 'sed' commands in below Format:
  ## sed -i "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE

  sed -i '' "s/StackTagValue/${STACK_TAG}/g" $PARAMETER_FILE
  sed -i '' "s/SubsystemValue/${SUBSYSTEM}/g" $PARAMETER_FILE
  sed -i '' "s%EnvironmentValue%${ENV}%g" $PARAMETER_FILE
  sed -i '' "s/Ec2AmiIdValue/${EC2_AMI_ID}/g" $PARAMETER_FILE
  sed -i '' "s%LoadBalancerSchemeValue%${ALB_SCHEME}%g" $PARAMETER_FILE
  sed -i '' "s%LoadBalancerProtocolValue%${ALB_PROTOCOL}%g" $PARAMETER_FILE
  sed -i '' "s%R53CreateFlagValue%${CREATE_R53}%g" $PARAMETER_FILE
  sed -i '' "s%R53HostedZoneIdValue%${R53_HOSTED_ZONE_ID}%g" $PARAMETER_FILE
  sed -i '' "s%R53RecordSetNameValue%${R53_RECORD_SET_NAME}%g" $PARAMETER_FILE
}

function cfn_deploy() {
  CFN_TEMPLATE_FILE=$1
  PARAMETER_FILE=$2

  cp "$PARAMETER_FILE" "$PARAMETER_FILE.bkp"

  echo "[[INFO]] Validating CFN template before proceeding: $1"
  aws cloudformation validate-template --region $REGION --template-body file://$CFN_TEMPLATE_FILE 1> $CFN_VALIDATE_LOG
  if [[ ! -s $CFN_VALIDATE_LOG ]]; then
    check_error 1 "\n[[ERROR]] CFN Validation Failed. Please check and try again."
  else
    echo -e "\n[[INFO]] CFN Successfully Validated. Proceeding with Script execution."
    rm -rf $CFN_VALIDATE_LOG

    CFN_STACK_NAME="$STACK_TAG-$REGION-$ENV-$SUBSYSTEM-stack"

    replace_template_parameters $PARAMETER_FILE
    
    sleep 10

    echo "[[INFO]] Checking if CFN Stack \"$CFN_STACK_NAME\" exists"
    CFN_STACK=$(aws cloudformation --region $REGION describe-stacks --stack-name "$CFN_STACK_NAME")

    if [[ -z $CFN_STACK ]]; then
      echo "[[INFO]] $CFN_STACK_NAME doesn't exist. Creating CFN stack for the same."
      aws cloudformation create-stack --region $REGION --template-body file://$CFN_TEMPLATE_FILE --parameters file://$PARAMETER_FILE --stack-name "$CFN_STACK_NAME" --tags file://$CFN_TAGS_FILE --capabilities CAPABILITY_NAMED_IAM --on-failure DO_NOTHING
    else
      echo "[[INFO]] $CFN_STACK_NAME CFN Stack already exists. Updating the stack."
      aws cloudformation update-stack --region $REGION --template-body file://$CFN_TEMPLATE_FILE --parameters file://$PARAMETER_FILE --stack-name "$CFN_STACK_NAME" --tags file://$CFN_TAGS_FILE --capabilities CAPABILITY_NAMED_IAM
    fi

    cp "$PARAMETER_FILE.bkp" $PARAMETER_FILE
    rm -f "$PARAMETER_FILE.bkp"
  fi
}

function check_cfn_stack_status() {
  echo "[[INFO]] Tracking CFN Stack (Create/Update) Progress. See below for details:"
  CFN_STACK_STATUS=$(aws cloudformation describe-stacks --region $REGION --stack-name $CFN_STACK_NAME | jq '.Stacks[].StackStatus' | cut -d '"' -f2)
  shopt -s nocasematch
  if [[ $CFN_STACK_STATUS =~ ^(CREATE|UPDATE)(_COMPLETE)$ ]]; then
    echo "[[INFO_$(date +%Y/%m/%d--%H:%M:%S)]] Status of $1 is \"$CFN_STACK_STATUS\". Proceeding further."
  elif [[ $CFN_STACK_STATUS =~ ^(CREATE|UPDATE|UPDATE_COMPLETE_CLEANUP)(_IN_PROGRESS)$ ]]; then
    echo "[[INFO_$(date +%Y/%m/%d--%H:%M:%S)]] Status of $1 is \"$CFN_STACK_STATUS\". Will check back in 1 minute."
    sleep 60
    check_cfn_stack_status $1
  else
    DEPLOYMENT_FAILED="true"
    echo "**[[ERROR]] Status of $1 is \"$CFN_STACK_STATUS\". Proceeding further. Please check the console."
  fi
  shopt -u nocasematch
  return
}

function cfn_stack_protection() {
  if [[ $1 == "enable" ]]; then
    echo "[[INFO]] Enabling Termination Protection for CFN Stack: \"$2\""
    aws cloudformation update-termination-protection --enable-termination-protection --region $3 --stack-name $2
  elif [[ $1 == "disable" ]]; then
    echo "[[INFO]] Disabling Termination Protection for CFN Stack: \"$2\""
    aws cloudformation update-termination-protection --no-enable-termination-protection --region $3 --stack-name $2
  else
    check_error 1 "[[ERROR]] Wrong Parameters passed for \"cfn_stack_protection()\" function."
  fi
}

function main() {
  validate_parameters "$@"
  convert_to_lower

  initialize

  echo "[[INFO]] Executing \"INFRA\" stack cfn deploy"
  cfn_deploy "$INFRA_CFN_TEMPLATE_FILE" "$INFRA_PARAMETER_FILE"
  check_error $? "[[ERROR]] Error while executing \"cfn_deploy\" function for $INFRA_CFN_TEMPLATE_FILE."
  check_cfn_stack_status $CFN_STACK_NAME
  cfn_stack_protection "enable" $CFN_STACK_NAME $REGION

  if [[ $DEPLOYMENT_FAILED == "false" ]]; then
    echo -e "\n==> SCRIPT EXECUTION IS \"SUCCESSFUL\". Please check CFN console for status and more details."
    return
  fi
}

main "$@"
