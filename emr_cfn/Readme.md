##### 1. Script Pre-Execution Requirements:
- Follow the placeholders and Update the CFN Parameters File in ./cfn/cfn_templates_parameters/infrastructure/env.account.region.emr-infra.cfn.parameters.json
- Update the CFN Stack Tags File as needed in ./cfn/cfn_templates_parameters/common/env.account.region.cfn-stack.tags.json
- Update both the Parameter and Stack Tags files name ( env.account.region.emr-infra.cfn.parameters.json and env.account.region.cfn-stack.tags.json ) with corresponding env/account/region values you need
- Update **sed** command under **replace_template_parameters()** function in the script as below,
  - For Mac UPDATE all 'sed' commands in below Format:
    > sed -i '' "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE
  - For Linux UPDATE all 'sed' commands in below Format:
    > sed -i "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE

##### 2. Software Requirements:
- Pleaes make sure **jq** and **aws cli** are installed in the machine where the scripts are being exeucted from.

##### 3. Execute Infrastructure Shell Script:
> Below example will create infra for **dev** environment and account **dataq** with EMR version **emr-5.30.1**

```bash
$ ./emr_infra_deploy.sh --stack-tag dataq --env dev --emr-version 'emr-5.30.1' --account dataq --region us-east-1
```