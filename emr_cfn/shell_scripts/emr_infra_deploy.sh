#!/bin/bash

declare STACK_TAG
declare ENV
declare EMR_VERSION

declare ACCOUNT
declare REGION

declare -r SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
declare -r SCALING_MINIMUM_CAPACITY_UNITS=25
declare -r SCALING_MAXIMUM_CAPACITY_UNITS=25
declare -r SCALING_MAXIMUM_ONDEMAND_CAPACITY_UNITS=25
declare -r SCALING_MAXIMUM_CORE_CAPACITY_UNITS=25
declare DEPLOYMENT_FAILED="false"
declare CFN_STACK_NAME
declare CFN_STACK_STATUS
declare EMR_CLUSTER_NAME

declare -r INFRA_CFN_TEMPLATE_FILE="$SCRIPTPATH/../cfn/cfn_templates/emr_infra_cfn.yml"
declare -r CFN_VALIDATE_LOG=success.log
declare PARAMETER_FILE
declare INFRA_PARAMETER_FILE
declare CFN_TAGS_FILE

function check_error() {
  if [ "${1}" -ne "0" ]; then
    DEPLOYMENT_FAILED="true"
    echo -e "ErrorCode#${1}; ${2} Exiting Script."
    exit ${1}
  else
    return
  fi
}

function validate_parameters() {
    while [[ "$#" > 0 ]]; do case $1 in
        --stack-tag) STACK_TAG="$2";;
        --env) ENV="$2";;
        --emr-version) EMR_VERSION="$2";;
        --account) ACCOUNT="$2";;
        --region) REGION="$2";;
        -h | --help) echo -e "\n[[USAGE]]\n$0 [--stack-tag <dataq>]* [--env <dev/sqa/prd>]* [--emr-version <emr-5.30.1>]* [--account <dataq>]* [--region <us-east-1>]* || [-h | --help]"
        echo -e "\n[[INFO]]\n\"*\" means Required Parameter\n\"**\" means Required parameter ONLY for certain Environments/Cases."
          exit 1;;
        *) echo "[[ERROR]] Argument \"$1 $2\" was unexpected. Execute script with -h or --help to see usage."
          exit 1;;
    esac; shift; shift
    done

    if [[ -z $STACK_TAG ]]; then
      check_error 1 "[[ERROR]] --stack-tag is a required parameter"
    fi
    if [[ -z $ENV ]]; then
      check_error 1 "[[ERROR]] --env is a required parameter"
    fi
    if [[ -z $EMR_VERSION ]]; then
      check_error 1 "[[ERROR]] --emr-version is a required parameter"
    fi
    if [[ -z $ACCOUNT ]]; then
      check_error 1 "[[ERROR]] --account is a required parameter"
    fi
    if [[ -z $REGION ]]; then
      check_error 1 "[[ERROR]] --region is a required parameter"
    fi
}

function initialize(){

  INFRA_PARAMETER_FILE="$SCRIPTPATH/../cfn/cfn_templates_parameters/infrastructure/$ENV.$ACCOUNT.$REGION.emr-infra.cfn.parameters.json"
  EMR_CLUSTER_NAME="$STACK_TAG-$REGION-$ENV-emr-cluster"
  CFN_TAGS_FILE="$SCRIPTPATH/../cfn/cfn_templates_parameters/common/$ENV.$ACCOUNT.$REGION.cfn-stack.tags.json"
}

function replace_template_parameters () {
  PARAMETER_FILE=$1

  ## For Mac UPDATE all 'sed' commands in below Format:
  ## sed -i '' "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE
  ## For Linux UPDATE all 'sed' commands in below Format:
  ## sed -i "s/ParameterNameValue/${PARAMETER_NAME}/g" $PARAMETER_FILE

  sed -i '' "s/StackTagValue/${STACK_TAG}/g" $PARAMETER_FILE
  sed -i '' "s%EnvironmentValue%${ENV}%g" $PARAMETER_FILE
  sed -i '' "s/EMRVersionValue/${EMR_VERSION}/g" $PARAMETER_FILE
  sed -i '' "s/EMRClusterNameValue/${EMR_CLUSTER_NAME}/g" $PARAMETER_FILE
}

function cfn_deploy() {
  CFN_TEMPLATE_FILE=$1
  PARAMETER_FILE=$2

  cp "$PARAMETER_FILE" "$PARAMETER_FILE.bkp"

  echo "[[INFO]] Validating CFN template before proceeding: $1"
  aws cloudformation validate-template --region $REGION --template-body file://$CFN_TEMPLATE_FILE 1> $CFN_VALIDATE_LOG
  if [[ ! -s $CFN_VALIDATE_LOG ]]; then
    check_error 1 "\n[[ERROR]] CFN Validation Failed. Please check and try again."
  else
    echo -e "\n[[INFO]] CFN Successfully Validated. Proceeding with Script execution."
    rm -rf $CFN_VALIDATE_LOG

    CFN_STACK_NAME="$STACK_TAG-$REGION-$ENV-emr-stack"

    replace_template_parameters $PARAMETER_FILE

    echo "[[INFO]] Checking if CFN Stack \"$CFN_STACK_NAME\" exists"
    CFN_STACK=$(aws cloudformation --region $REGION describe-stacks --stack-name "$CFN_STACK_NAME")

    if [[ -z $CFN_STACK ]]; then
      echo "[[INFO]] $CFN_STACK_NAME doesn't exist. Creating CFN stack for the same."
      aws cloudformation create-stack --region $REGION --template-body file://$CFN_TEMPLATE_FILE --parameters file://$PARAMETER_FILE --stack-name "$CFN_STACK_NAME" --tags file://$CFN_TAGS_FILE --capabilities CAPABILITY_NAMED_IAM --on-failure DO_NOTHING
    else
      echo "[[INFO]] $CFN_STACK_NAME CFN Stack already exists. Updating the stack."
      aws cloudformation update-stack --region $REGION --template-body file://$CFN_TEMPLATE_FILE --parameters file://$PARAMETER_FILE --stack-name "$CFN_STACK_NAME" --tags file://$CFN_TAGS_FILE --capabilities CAPABILITY_NAMED_IAM
    fi

    cp "$PARAMETER_FILE.bkp" $PARAMETER_FILE
    rm -f "$PARAMETER_FILE.bkp"
  fi
}

function check_cfn_stack_status() {
  echo "[[INFO]] Tracking CFN Stack (Create/Update) Progress. See below for details:"
  CFN_STACK_STATUS=$(aws cloudformation describe-stacks --region $REGION --stack-name $CFN_STACK_NAME | jq '.Stacks[].StackStatus' | cut -d '"' -f2)
  shopt -s nocasematch
  if [[ $CFN_STACK_STATUS =~ ^(CREATE|UPDATE)(_COMPLETE)$ ]]; then
    echo "[[INFO_$(date +%Y/%m/%d--%H:%M:%S)]] Status of $1 is \"$CFN_STACK_STATUS\". Proceeding further."
  elif [[ $CFN_STACK_STATUS =~ ^(CREATE|UPDATE|UPDATE_COMPLETE_CLEANUP)(_IN_PROGRESS)$ ]]; then
    echo "[[INFO_$(date +%Y/%m/%d--%H:%M:%S)]] Status of $1 is \"$CFN_STACK_STATUS\". Will check back in 1 minute."
    sleep 60
    check_cfn_stack_status $1
  else
    DEPLOYMENT_FAILED="true"
    echo "**[[ERROR]] Status of $1 is \"$CFN_STACK_STATUS\". Proceeding further. Please check the console."
  fi
  # return
}

function cfn_stack_protection() {
  if [[ $1 == "enable" ]]; then
    echo "[[INFO]] Enabling Termination Protection for CFN Stack: \"$2\""
    aws cloudformation update-termination-protection --enable-termination-protection --region $3 --stack-name $2
  elif [[ $1 == "disable" ]]; then
    echo "[[INFO]] Disabling Termination Protection for CFN Stack: \"$2\""
    aws cloudformation update-termination-protection --no-enable-termination-protection --region $3 --stack-name $2
  else
    check_error 1 "[[ERROR]] Wrong Parameters passed for \"cfn_stack_protection()\" function."
  fi
}

function put_managed_policy() {
  local EMR_CLUSTER_ID
  CFN_STACK_STATUS=$(aws cloudformation describe-stacks --region $REGION --stack-name $CFN_STACK_NAME | jq '.Stacks[].StackStatus' | cut -d '"' -f2)
  if [[ $CFN_STACK_STATUS =~ ^(CREATE|UPDATE)(_COMPLETE)$ ]]; then
    echo "[[INFO]] CFN Stack Status is: \"$CFN_STACK_STATUS\". Putting Managed Scaling Policy on EMR Cluster."
    EMR_CLUSTER_ID=$(aws emr list-clusters --region $REGION --cluster-states WAITING | jq --arg v "$EMR_CLUSTER_NAME" '.Clusters[] | select(.Name == $v) | .Id' | cut -d '"' -f2)
    if [[ -z $EMR_CLUSTER_ID ]]; then
      check_error 1 "\n\n**[[ERROR]] Unable to get EMR Cluster ID for Cluster: \"$EMR_CLUSTER_NAME\". Cluster scaling CANNOT be Enabled. Please verify with PRIORITY."
    fi
    aws emr put-managed-scaling-policy --region $REGION --cluster-id $EMR_CLUSTER_ID --managed-scaling-policy ComputeLimits='{MinimumCapacityUnits='$SCALING_MINIMUM_CAPACITY_UNITS', MaximumCapacityUnits='$SCALING_MAXIMUM_CAPACITY_UNITS', MaximumOnDemandCapacityUnits='$SCALING_MAXIMUM_ONDEMAND_CAPACITY_UNITS', MaximumCoreCapacityUnits='$SCALING_MAXIMUM_CORE_CAPACITY_UNITS', UnitType=Instances}'
    check_error $? "\n\n**[[ERROR]] Error while putting Managed Policy for Cluster: \"$EMR_CLUSTER_NAME\". Cluster scaling CANNOT be Enabled. Please verify with PRIORITY."
  else
    check_error 1 "\n\n**[[ERROR]] CFN Stack Status is in Unexpected State (\"$CFN_STACK_STATUS\"). Cluster scaling CANNOT be Enabled. Please verify with PRIORITY."
  fi
}

function main() {
  validate_parameters "$@"

  initialize

  echo "[[INFO]] Executing \"INFRA\" stack cfn deploy"
  cfn_deploy "$INFRA_CFN_TEMPLATE_FILE" "$INFRA_PARAMETER_FILE"
  check_error $? "[[ERROR]] Error while executing \"cfn_deploy\" function for $INFRA_CFN_TEMPLATE_FILE."
  check_cfn_stack_status $CFN_STACK_NAME
  cfn_stack_protection "enable" $CFN_STACK_NAME $REGION

  put_managed_policy

  if [[ $DEPLOYMENT_FAILED == "false" ]]; then
    echo -e "\n==> SCRIPT EXECUTION IS \"SUCCESSFUL\". Please check CFN console for status and more details."
    return
  fi
}

main "$@"
